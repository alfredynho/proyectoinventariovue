import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Area from '../views/Area.vue'
import AreaEdit from '../views/AreaEdit.vue'
import Asignacion from '../views/Asignacion.vue'
import AsignacionEdit from '../views/AsignacionEdit.vue'


const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/area',
    name: 'area',
    component: Area
  },
  {
    path: '/area/:id',
    name: 'areaedit',
    component: AreaEdit
  },
  {
    path: '/asignaciones',
    name: 'asignaciones',
    component: Asignacion
  },
  {
    path: '/asignacion/:id',
    name: 'asignacionedit',
    component: AsignacionEdit
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
