## Proyecto Diplomado Inventario 

### Tecnologías

  * [Vuejs](https://vuejs.org/)

### Integrantes del Grupo

  - `Luis Angel Quispe Limachi`
  - `Alfredo Callizaya Gutierrez`

### Comandos para Levantar el Proyecto

## Instalar Librerias
```
npm install
```
## Configurar entorno
```
mv .env.example .env
```
### Levantar la BD JSON-Server en escucha
```
json-server --watch db.json
```
### Levantar el Servidor
```
npm run serve
```